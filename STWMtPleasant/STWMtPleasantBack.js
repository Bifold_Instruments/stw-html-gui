﻿const writeposurl = process.env.URL_WritePosition;
const readpositionurl = process.env.URL_ReadPosition;
const readtorqueurl = process.env.URL_ReadTorque;
const readpvcellurl = process.env.URL_ReadPVCell;
const readbatturl = process.env.URL_ReadBattery;
const readactstatusurl = process.env.URL_ReadStatus;
//0=na, 1=pos, 2=trq, 3=bat, 4=pvc, 5=net
var leftChartFlag = 0;																									//keep track of which chart is in left graph
var rightChartFlag = 0;																									//keep track of which chart is in right graph

// The open service via ThingSpeak.com has a rate limit 
// of an update per channel every 15 seconds.

async function setActPosition(positionPerc) {
	const url = writeposurl + positionPerc;
	const response = await fetch(url);
	const success = await response.json();
}

async function getPosData(url, dpNum) {
	var actPosData = [];
	var desPosData = [];

	const response = await fetch(url);
	const data = await response.json()
		.catch(error => {
			console.error(error);
		})
	const { name, id, field1, field8 } = data.channel;

	var timeVal = data.feeds.map(function (e) {
		return new Date(e.created_at);
	});
	var aPD = data.feeds.map(function (e) {
		return e.field1;
	});
	var dPD = data.feeds.map(function (e) {
		return e.field8;
	});

	for (var i = 0; i < dpNum; i++) {
		if ((aPD[i] == null) && (aPD[i - 1] != null)) {
			aPD[i] = aPD[i - 1];
		}
		if ((dPD[i] == null) && (aPD[i - 1] != null)) {
			dPD[i] = dPD[i - 1];
		}
		actPosData.push({
			x: timeVal[i],
			y: aPD[i]
		});
		desPosData.push({
			x: timeVal[i],
			y: dPD[i]
		})
	}

	return { field1, field8, actPosData, desPosData };
}

async function getTrqData(url, dpNum) {
	var actTrqData = [];

	const response = await fetch(url);
	const data = await response.json()
		.catch(error => {
			console.error(error);
		})
	const { name, id, field2 } = data.channel;

	var timeVal = data.feeds.map(function (e) {
		return new Date(e.created_at);
	});
	var aTD = data.feeds.map(function (e) {
		return e.field2;
	});

	for (var i = 0; i < dpNum; i++) {
		if ((aTD[i] == null) && (aTD[i - 1] != null)) {
			aTD[i] = aTD[i - 1];
		}
		actTrqData.push({
			x: timeVal[i],
			y: aTD[i]
		});
	}

	return { field2, actTrqData };
}

async function getBatData(url, dpNum) {
	var batVtgData = [];

	const response = await fetch(url);
	const data = await response.json()
		.catch(error => {
			console.error(error);
		})
	const { name, id, field3 } = data.channel;

	var timeVal = data.feeds.map(function (e) {
		return new Date(e.created_at);
	});
	var bVD = data.feeds.map(function (e) {
		return e.field3;
	});

	for (var i = 0; i < dpNum; i++) {
		batVtgData.push({
			x: timeVal[i],
			y: bVD[i]
		});
	}

	return { field3, batVtgData };
}

async function getPvcData(url, dpNum) {
	var pvCellData = [];

	const response = await fetch(url);
	const data = await response.json()
		.catch(error => {
			console.error(error);
		})
	const { name, id, field2 } = data.channel;

	var timeVal = data.feeds.map(function (e) {
		return new Date(e.created_at);
	});
	var pvC = data.feeds.map(function (e) {
		return e.field2;
	});

	for (var i = 0; i < dpNum; i++) {
		pvCellData.push({
			x: timeVal[i],
			y: pvC[i]
		});
	}

	return { field2, pvCellData };
}

async function drawPosGraph(dpNum, graphPosition) {
	var url = readpositionurl + dpNum;
	var posData = await getPosData(url, dpNum);

	const config = {
		type: 'line',
		data: {
			datasets: [{
				label: posData.field8,
				fill: false,
				data: posData.desPosData,
				backgroundColor: 'rgba(255, 99, 132, 0.2)',
				borderColor: 'rgba(255, 99, 132, 1)',
				borderWidth: 1,
				lineTension: 0
			}, {
				label: posData.field1,
				fill: false,
				data: posData.actPosData,
				backgroundColor: 'rgba(54, 162, 235, 0.2)',
				borderColor: 'rgba(54, 162, 235, 1)',
				borderWidth: 1,
				lineTension: 0
			}]
		},
		options: {
			elements: {
				point: {
					radius: 0
				}
			},
			title: {
				display: true,
				text: 'Actuator Position Chart'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					type: 'time',
					time: {
						unit: 'day'
					},
					distribution: 'linear',
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Date'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: '%'
					},
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	};
	const ctx = document.getElementById(graphPosition).getContext('2d');
	if (graphPosition == 'leftGraph') {
		leftChartFlag = 1;
		lposChart = new Chart(ctx, config);
	}
	else if (graphPosition == 'rightGraph') {
		rightChartFlag = 1;
		rposChart = new Chart(ctx, config);
	}
}

async function drawTrqGraph(dpNum, graphPosition) {
	var url = readtorqueurl + dpNum;
	var trqData = await getTrqData(url, dpNum);

	const config = {
		type: 'line',
		data: {
			datasets: [{
				label: trqData.field2,
				backgroundColor: 'rgba(227, 143, 18, 0.2)',
				borderColor: 'rgba(227, 143, 18, 1)',
				borderWidth: 1,
				data: trqData.actTrqData,
				fill: false,
			}]
		},
		options: {
			elements: {
				point: {
					radius: 0
				}
			},
			title: {
				display: true,
				text: 'Actuator Torque Chart'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					type: 'time',
					time: {
						unit: 'day'
					},
					distribution: 'linear',
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Date'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Value'
					}
				}]
			}
		}
	};

	const ctx = document.getElementById(graphPosition).getContext('2d');
	if (graphPosition == 'leftGraph') {
		leftChartFlag = 2;
		ltrqChart = new Chart(ctx, config);
	}
	else if (graphPosition == 'rightGraph') {
		rightChartFlag = 2;
		rtrqChart = new Chart(ctx, config);
	}
}


async function drawBatGraph(dpNum, graphPosition) {
	var url = readbatturl + dpNum;
	var batData = await getBatData(url, dpNum);

	const config = {
		type: 'line',
		data: {
			datasets: [{
				label: batData.field3,
				backgroundColor: 'rgba(168, 220, 116, 0.2)',
				borderColor: 'rgba(168, 220, 116, 1)',
				borderWidth: 1,
				borderDash: [5, 5],
				data: batData.batVtgData,
				fill: false,
			}]
		},
		options: {
			elements: {
				point: {
					radius: 0
				}
			},
			title: {
				display: true,
				text: 'Battery Voltage Chart'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					type: 'time',
					time: {
						unit: 'day'
					},
					distribution: 'linear',
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Date'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Value'
					}
				}]
			}
		}
	};

	const ctx = document.getElementById(graphPosition).getContext('2d');
	if (graphPosition == 'leftGraph') {
		leftChartFlag = 3;
		lbatChart = new Chart(ctx, config);
	}
	else if (graphPosition == 'rightGraph') {
		rightChartFlag = 3;
		rbatChart = new Chart(ctx, config);
	}
}

async function drawPVCGraph(dpNum, graphPosition) {
	var url = readpvcellurl + dpNum;
	var pvcData = await getPvcData(url, dpNum);

	const config = {
		type: 'line',
		data: {
			datasets: [{
				label: pvcData.field2,
				backgroundColor: 'rgba(150, 18, 227, 0.2)',
				borderColor: 'rgba(150, 18, 227, 1)',
				borderWidth: 1,
				data: pvcData.pvCellData,
				fill: false,
			}]
		},
		options: {
			elements: {
				point: {
					radius: 0
				}
			},
			title: {
				display: true,
				text: 'PV Cell Power Chart'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					type: 'time',
					time: {
						unit: 'day'
					},
					distribution: 'linear',
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Date'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Value'
					}
				}]
			}
		}
	};

	const ctx = document.getElementById(graphPosition).getContext('2d');
	if (graphPosition == 'leftGraph') {
		leftChartFlag = 4;
		lpvcChart = new Chart(ctx, config);
	}
	else if (graphPosition == 'rightGraph') {
		rightChartFlag = 4;
		rpvcChart = new Chart(ctx, config);
	}
}

async function getActuatorStatus() {
	var status = "";
	var stattime = "";
	const response = await fetch(readactstatusurl);
	const data = await response.json()
		.catch(error => {
			console.error(error);
		})
	if (data.feeds.length != 0) {
		status = data.feeds[0].status;
		stattime = new Date(data.feeds[0].created_at).toLocaleString();
	}
	else {
		status = "No Status Available";
		stattime = new Date().toLocaleString();
	}

	document.getElementById("actStatus").innerHTML = status;
	document.getElementById("statTime").innerHTML = stattime;
}

function refreshPage() {
	console.log('refreshing page');
	changeDataPoints();
	getActuatorStatus();
}

function openGraph(graphType, graphPosition) {
	if (graphPosition == 'leftGraph') {
		switch (leftChartFlag) {
			case 1: lposChart.destroy(); break;
			case 2: ltrqChart.destroy(); break;
			case 3: lbatChart.destroy(); break;
			case 4: lpvcChart.destroy(); break;
		}
	}
	else if (graphPosition == 'rightGraph') {
		switch (rightChartFlag) {
			case 1: rposChart.destroy(); break;
			case 2: rtrqChart.destroy(); break;
			case 3: rbatChart.destroy(); break;
			case 4: rpvcChart.destroy(); break;
		}
	}
	var dp = document.forms["graphform"]["datapointsVal"].value;

	switch (graphType) {
		case 1: drawPosGraph(dp, graphPosition); break;
		case 2: drawTrqGraph(dp, graphPosition); break;
		case 3: drawBatGraph(dp, graphPosition); break;
		case 4: drawPVCGraph(dp, graphPosition); break;
		case 5: drawNetGraph(dp, graphPosition); break;
	}
}

//recall api functions to get graph data with new results values
function changeDataPoints() {
	openGraph(leftChartFlag, 'leftGraph');
	openGraph(rightChartFlag, 'rightGraph');
}

//add sanity check before sending API call
function clickConfirm() {
	var empt = document.forms["controlform"]["posPerc"].value;
	if (empt == "") {
		alert("Please input a value");
		return false;
	}
	else if ((empt > 100) || (empt < 0)) {
		alert("Input Value out of range");
		return false;
	}
	else {
		var r = confirm("Are you sure?");
		if (r == true) {
			setActPosition(empt);
		}
		else {
			return;
		}
	}
}

window.onload = function () {
	drawPosGraph(250, 'leftGraph');
	drawBatGraph(250, 'rightGraph');
	getActuatorStatus();
}